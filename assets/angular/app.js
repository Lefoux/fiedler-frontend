angular.module("fiedlerApp", [ "ngSanitize" ])
.controller("fiedlerAppCtrl", function($scope, $http, $window) {

	$scope.workers = null;
	$scope.attRep = null;
	$scope.repAlert = null;
	$scope.attError = null;
	$scope.wcourant = null;
 
	$http.get('http://localhost/fiedler-lare/server/worker.class.php?action=all')
	.then(function(response) {
		$scope.workers = response.data.records;
	}, function myError(response) {
        $scope.walError = response.statusText;
    });

   	$scope.addAttendance = function(worker) {
    	$http.get('http://localhost/fiedler-lare/server/attendance.class.php?action=add&worker='+worker.id+'')
		.then(function(response) {
			$scope.attRep = response.data.rep;
			$scope.repAlert = $scope.alertBox($scope.attRep);
			$scope.wcourant = worker;
		}, function myError(response) {
	        $scope.attError = response.statusText;
	    });
    }

    $scope.alertBox = function(arg) {
    	$scope.alertMsg = 'Echec : Validation échoué.!';
    	//$scope.alertTyp = '#d4d4d4';

    	if (arg == 'updated') {
    		$scope.alertMsg = 'Succès : Au revoir, bon retour.!';
    		//$scope.alertTyp = '#5cb85c';
		} else if(arg == 'created') {
			$scope.alertMsg = 'succès : Bienvenue au service.!';
			//$scope.alertTyp = '#eb9316';	
		}

		return $window.alert($scope.alertMsg);
    }

});