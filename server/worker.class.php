<?php

require_once 'db/dbconf.php';

class WORKER
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
	public function lasdID()
	{
		$stmt = $this->conn->lastInsertId();
		return $stmt;
	}

	public function all()
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT * FROM workers where status = 1");
			$stmt->execute();
			$result = $stmt->fetchAll();
			
			$outp = "";
			foreach ($result as $key => $rs) {
				if ($outp != "") {$outp .= ",";}
				$outp .= '{"id":"'.$rs["id"].'",';
			     $outp .= '"lastname":"'.$rs["lastname"].'",';
			     $outp .= '"firstname":"'.$rs["firstname"].'",';
			     $outp .= '"sexe":"'.$rs["sexe"].'",';
			     $outp .= '"datenaiss":"'.$rs["datenaiss"].'",';
			     $outp .= '"phone":"'.$rs["phone"].'",';
			     $outp .= '"email":"'.$rs["email"].'"}';
			}
			$outp ='{"records":['.$outp.']}';
			 
			echo $outp;
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	public function add($datas)
	{
		try
		{							
			
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	public function update($id)
	{
		try
		{
			
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}

}


$worker = new WORKER();

if (isset($_GET['action'])) {
	$action = $_GET['action'];
	if ($action == 'all') {
		return $worker->all();
	} elseif ($action == 'update') {
		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			return $worker->update($id);
		} else {
			return 'Request error except.!';
		}
	} elseif ($action == 'add') {
		return $worker->add();
	}
}