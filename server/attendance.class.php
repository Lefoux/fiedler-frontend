<?php

require_once 'db/dbconf.php';

class ATTENDANCE
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
	public function lasdID()
	{
		$stmt = $this->conn->lastInsertId();
		return $stmt;
	}
	
	public function add($worker)
	{
		$today = date('Y-m-d');

		try
		{							
			$stmt = $this->conn->prepare("SELECT * FROM attendance WHERE worker_id=:xworker AND date_work=:xdate");
			$stmt->execute(array(
				":xworker" => $worker,
				":xdate" => $today
			));
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			if (isset($result['worker_id'])) {
				$stmt = $this->conn->prepare("UPDATE attendance SET heure_depart=:xheure WHERE id=:xid");
				$rep = $stmt->execute(array(
					":xheure" => gmdate('Y-m-d H:i:s'),
					":xid" => $result['id']
				));
				if ($rep) {
					echo '{"rep":"updated"}';
				}
			} else {
				$stmt = $this->conn->prepare("INSERT INTO attendance (date_work,heure_arrivee,worker_id) VALUES(:xdate, :xheure, :xworker)");
				$rep = $stmt->execute(array(
					":xdate" => $today,
					":xheure" => gmdate('Y-m-d H:i:s'),
					":xworker" => $worker
				));
				if ($rep) {
					echo '{"rep":"created"}';
				}
			}
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	public function update($email,$upass)
	{
		try
		{
			
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}
}


$attendance = new ATTENDANCE();

if (isset($_GET['action'])) {
	$action = $_GET['action'];
	if ($action == 'all') {
		return $attendance->get();
	} elseif ($action == 'add') {
		if (isset($_GET['worker'])) {
			$worker = $_GET['worker'];
			return $attendance->add($worker);
		} else {
			return 'Request error except.!';
		}
	} elseif ($action == 'update') {
		return $attendance->add($id);
	}
}